export class Dish {
    id: string | undefined;
    name!: string;
    image!: string;
    category!: string;
    featured!: boolean;
    label!: string;
    price!: string;
    description!: string;
}